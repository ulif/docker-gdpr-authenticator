# Build Senfcall GDPR Authenticator
FROM ubuntu:16.04
MAINTAINER ulif <uli@gnufix.de>

# update base, install packages we need
RUN apt update \
  && apt install -y -q --no-install-recommends \
     build-essential ca-certificates curl git \
  && apt-get clean && rm -r /var/lib/apt/lists/*

# install rust
RUN mkdir /build
WORKDIR /build
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs > rustup.sh
RUN sh rustup.sh -y

ENV PATH="/root/.cargo/bin:${PATH}"

# get gdpr-authenticator sources and build
RUN git clone https://gitlab.senfcall.de/senfcall-public/gdpr-authenticator /build/gdpr-authenticator
WORKDIR /build/gdpr-authenticator
RUN cargo install cargo-deb
RUN cargo deb

# copy over results to /r
RUN mkdir /r && cp target/debian/gdpr*.deb /r
