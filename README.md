# Docker GDPR Authenticator

Docker creating the [Senfcall GDPR Authenticator](https://gitlab.senfcall.de/senfcall-public/gdpr-authenticator) when run.

The GDPR Authenticator was made for BigBlueButtonm installs to ask for consent
before people enter any conferences. It runs as an own service, called by
nginx. See the above link to learn more.

This little script builds the authenticator in an encapsulated docker
environment, which guarantees to provide a proper OS (Ubuntu 16.04) and enables
to remove all build-env after use easily.


## Build the Debian package

Clone this package and change into it. Then start the build

    $ git clone https://gitlab.digitalcourage.de/ulif/docker-gdpr-authenticator
    $ cd docker-gdpr-authenticator
    $ docker build -t gdpr-auth .

(please note the trailing dot).

This will take some time, build the authenticator in a Ubuntu 16.04 environment
and finally store the resulting Debian package in the root dir `/r` inside the
docker container.


## Copy the resulting package from the container

    $ docker run --rm -v `pwd`:/foo gdpr-authenticator cp -r /r /foo

and you will find a new directory `r` containing a `.deb` package in your
current directory. On the target server you can install the package running


    $ dpkg -i <PKG-NAME>


## Clean up

You can remove everything afterwards to free resiources.:

    $ docker rmi gdpr-authenticator

You might want to repeat the procedure from time to time to update the generated package. As time of writing version 0.1.4 was current.

Have fun.
